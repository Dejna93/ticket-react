import UserStore from '../stores/UserStore.js';
import TicketStore from '../stores/TicketStore.js';
import { browserHistory } from 'react-router'

export function logout(){
  UserStore.cleanUser();
  TicketStore.cleanStore();
  browserHistory.push("/");

}

export function colorTable(state){
  if(state =='Zamknięty') return 'danger';
  if(state =='Opóźnione') return 'warning';
  if(state =='Nowy') return 'success';
  return '';
}
