// User API logic
import { login , logout , error, view} from '../actions/UserServerActions';
import axios from 'axios';
export function loginAPI(data) {
  axios.post('http://localhost:3000/api/users/login/',data)
    .then(function(response){
      console.info(response); // ex.: { user: 'Your User'}
      console.log("STATUSEK ="+response.status); // ex.: 200
      login(response.data,data.password);
    }).catch(function(error){
      if (error.response) {
        console.log("STATUS ="+error.response.status);
        login(error.response.status);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
        error(error.response.status);
      }
    });

}

export function viewUseAPI(username){

    axios.get('http://localhost:3000/api/users/'+username+"/")
    .then(function(response){
        view(response);
    }).catch(function(err){
      console.log("ERROR ="+err );
    });
}

//  request.post('http://localhost:3000/api/users/login/')
//    .set('Content-Type', 'application/json')
//    .send( {
//      'username': 'dejna',
//      'password': 'zaq1@WSX',
  //  })
  //  .end((err, response) => {
  //    if (err) return console.error(err);
  //    console.info(response.body);
      //login(response.body);
    //});

//TODO W DJANGO ZRÓB LOGOUT :D OBSŁUŻ W JS
