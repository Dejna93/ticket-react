import {receiveTicketsAPI ,receiveTicketAPI ,receiveArchiveAPI ,addTicketServer, deleteTicketServer, updateTicketServer} from '../actions/TicketServerActions.js';
import axios from 'axios';
import stringify from 'json-stringify-safe';

export function getAllTicketsAPI(){
    axios.get('http://localhost:3000/api/tickets/')
    .then(function(response){
    //  console.info(response);
      receiveTicketsAPI(response.data);
    })
    .catch(function(err){
      console.error(err);
    });
}

export function getTicketsByStateAPI(state){
    axios.get('http://localhost:3000/api/tickets/', {
      params: {
        state: state
      }
    })
    .then(function(response){
    //  console.info(response);
      receiveTicketsAPI(response.data);
    })
    .catch(function(err){
      console.error(err);
    });
}
export function getTicketsByUserAPI(user){
    axios.get('http://localhost:3000/api/tickets/', {
      params: {
        user : user
      }
    })
    .then(function(response){
    //  console.info(response);
      receiveTicketsAPI(response.data);
    })
    .catch(function(err){
      console.error(err);
    });
}

export function getTicketByID(id){
  axios.get('http://localhost:3000/api/tickets/'+id+"/")
    .then(function(response){
        receiveTicketAPI(response);
    })
    .catch(function(err){
      console.error(err);
    })
}
export function getArchiveTicketsAPI(user){
    axios.get('http://localhost:3000/api/tickets/', {
      params: {
        username : user,
        archive : "tak",
      }
    })
    .then(function(response){
    //  console.info(response);
      receiveArchiveAPI(response.data);
    })
    .catch(function(err){
      console.error(err);
    });
}
export function deleteTicketAPI(id){
  console.info('Delete');
  axios.delete('http://localhost:3000/api/tickets/'+id+'/delete/')
    .then(function(){
  //    console.info('response ' + response);
      deleteTicketServer(response);
    })
    .catch(function(err){
      console.error('Error '+err);
    });
}
export function createTicketAPI(data){

  axios.post('http://localhost:3000/api/tickets/create/' , data)
  .then(function(){
//    console.info(response);
    addTicketServer(response);
  })
  .catch(function(err){
    console.error('ERROR ='+err);
  });
}

export function updateTicketAPI(data,auth,user){

  axios.put('http://localhost:3000/api/tickets/'+data.id+'/edit/', data)
    .then(function(){
      console.response(response);
      updateTicketServer(response);
    })
    .catch(function(err){
      console.error(err);
    });
}
