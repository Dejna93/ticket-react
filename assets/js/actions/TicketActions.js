import AppDispatcher from '../dispatcher/AppDispatcher';
import {TicketConstants} from '../constants/TicketConstants';
import { getAllTicketsAPI , getTicketByID , createTicketAPI, deleteTicketAPI ,   updateTicketAPI} from '../utils/TicketAPI.js';
import {getTicketsByStateAPI ,getTicketsByUserAPI } from '../utils/TicketAPI.js';

export function getTickets(user,state){
  AppDispatcher.handleViewAction({
    actionType: TicketConstants.GET_ALL_TICKETS
  });
  getAllTicketsAPI();
}
export function getTicketByState(state){
  AppDispatcher.handleViewAction({
    actionType: TicketConstants.GET_ALL_TICKETS
  });
  getTicketsByStateAPI(state);
}
export function getTicketsByUser(user){
  AppDispatcher.handleViewAction({
    actionType: TicketConstants.GET_ALL_TICKETS
  });
  getTicketsByUserAPI(user);
}
export function getArchives(user){
  AppDispatcher.handleViewAction({
    actionType: TicketConstants.GET_ARCHIVES
  });
  getArchiveTicketsAPI(user);
}

export function getTicket(id){
  AppDispatcher.handleViewAction({
    actionType : TicketConstants.GET_TICKET
  });
  getTicketByID(id);
}

//funkcja zapisu ticketa mówiąca dispatcherowi że
//musi obsłużyć akcje widoku która przyjmuje typ DOdania z argumentem data (nasz form)
export function saveTicket(data,user) {
  AppDispatcher.handleViewAction({
    actionType: TicketConstants.ADD_TICKET,
    data : data,
  });
  data["user"] = user;
  createTicketAPI(data);
}
export function updateTicket(ticket,auth, user){
  AppDispatcher.handleViewAction({
    actionType : TicketConstants.UPDATE_TICKET,
  //  data : ticket,
  });

  updateTicketAPI(ticket,auth,user);
}

export function deleteTicket(id) {
  AppDispatcher.handleViewAction({
    actionType: TicketConstants.DELETE_TICKET,
    index :id,
  });
  deleteTicketAPI(id);
}
