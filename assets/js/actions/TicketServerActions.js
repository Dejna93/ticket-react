// Todo actions
import AppDispatcher from '../dispatcher/AppDispatcher';
import { TicketConstants } from '../constants/TicketConstants';
export function receiveTicketsAPI(response) {
  AppDispatcher.handleServerAction({
    actionType: TicketConstants.GET_ALL_TICKETS,
    response: response,
  });
}
export function receiveTicketAPI(response) {
  AppDispatcher.handleServerAction({
    actionType: TicketConstants.GET_TICKET,
    response: response,
  });
}

export function receiveArchiveAPI(response) {
  AppDispatcher.handleServerAction({
    actionType: TicketConstants.GET_ARCHIVES,
    response: response,
  });
}
export function addTicketServer(response) {
  AppDispatcher.handleServerAction({
    actionType: TicketConstants.ADD_TICKET,
    response: response,
  });
}
export function updateTicketServer(response) {
  AppDispatcher.handleServerAction({
    actionType: TicketConstants.UPDATE_TICKET,
    response: response,
  });
}
export function deleteTicketServer(response) {
  AppDispatcher.handleServerAction({
    actionType: TicketConstants.DELETE_TICKET,
    response: response,
  });
}
export function fetchFormAPI(response) {
  AppDispatcher.handleServerAction({
    actionType: TicketConstants.FETCH_FORM,
    response: response,
  });
}
