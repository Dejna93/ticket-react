// User actions
import AppDispatcher from '../dispatcher/AppDispatcher';
import {UserConstants} from '../constants/UserConstants';
import {loginAPI, viewUseAPI} from '../utils/UserAPI.js';

export function loginUser(data){
  AppDispatcher.handleViewAction({
    actionType: UserConstants.LOGIN,
    data : data,
  });
  console.info(data);
  //send request to the server to create token
  loginAPI(data);
}

export function logoutUser(){
  AppDispatcher.handleViewAction({
    actionType: UserConstants.LOGIN,
  });
}

export function viewUser(username){
  viewUseAPI(username);
}

//export default function logoutUser(data){
///  AppDispatcher.handleViewAction({
//    actionType: UserConstants.LOGOUT,
//    data: data,
//  });
  //send request api to the server to destroy session
//  logout(data);
//}
