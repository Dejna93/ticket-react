// Todo actions
import AppDispatcher from '../dispatcher/AppDispatcher';
import {UserConstants} from '../constants/UserConstants';


export function login(response,data) {
  console.info('SERVERACTION' +response);
  if (response == "400"){
    AppDispatcher.handleServerAction({
      actionType: UserConstants.ERROR,
      response : response,
    });
  }else{
  AppDispatcher.handleServerAction({
    actionType: UserConstants.LOGIN_API,
    response: response,
    data : data,
  });
}
}

export function logout(response){
  AppDispatcher.handleServerAction({
    actionType: UserConstants.LOGOUT_API,
    response : response,
  });
}

export function error(error){
  console.info('ERROR');
}

export function view(response){
  AppDispatcher.handleServerAction({
    actionType : UserConstants.USER,
    response : response,
  });
}
