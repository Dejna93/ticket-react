export const UserConstants = {
  LOGIN : 'LOGIN',
  LOGOUT :'LOGOUT',
  LOGIN_API : 'LOGIN_API',
  LOGOUT_API : 'LOGOUT_API',
  SAVE : 'SAVE',
  ERROR : 'ERROR',
  USER : 'USER',
};
