import AppDispatcher from '../dispatcher/AppDispatcher';
import { TicketConstants } from '../constants/TicketConstants';
import { EventEmitter } from 'events';
import {getTickets} from '../actions/TicketActions.js';
const CHANGE_EVENT = 'change';

//lokalna zmienna JSONa w której bedziemy trzymać nasze tickety Array
let _store = {
  tickets : [],
};

class TicketStoreClass extends EventEmitter {


  //funkcja która dodajemy do componenta w którym bedziemy obsługiwać
  //zmiany dla tego magazynu
  //czyli componentDidMount() oraz componentWillUnmount()
  //do zmian w magazynie w compontentach bedzie służyla funkcja setState( z argumentem naszego magazynu)
  addChangeListener(cb) {
    this.on(CHANGE_EVENT, cb);
  }

  removeChangeListener(cb) {
    this.removeListener(CHANGE_EVENT, cb);
  }
  initTicket(){
    return {
      id: '',
      title: '',
      state : '',
      type : '',
      content :'',
      create : '',
      user : '',
    };
  }

  getAllTickets() {
    return _store.tickets;
  }
  getTicket(id){

  return _store.tickets[id];
    //if(id !== _store.tickets.length)
    //  return _store.tickets[id];
    //else
    //  return {};
  }

  getToken(){
    return _store.tickets.token;
  }

  //GETTER
  getStore(){
    return _store.tickets;
  }
  cleanStore(){
    _store.tickets =[];
  }
}
const TicketStore = new TicketStoreClass();

//Obsługa akcji wywołanych w TicketActions
//na podstawie action.actionType robimy odpowiednie działanie na magazynie
AppDispatcher.register((payload) => {
  const action = payload.action;

  switch (action.actionType) {

    //to działa ale nie podpięte
    case TicketConstants.GET_ALL_TICKETS:

      if(action.response){
        _store.tickets.splice(0,_store.tickets.length);
          for ( var i in action.response){
            //var ticket = JSON.stringify(action.response[i]);
            var ticket = action.response[i];
            _store.tickets.push(ticket);

          }
      }
      TicketStore.emit(CHANGE_EVENT);
    break;

    case TicketConstants.GET_TICKET:
      if(action.response){
        for(var i in _store.tickets){
          if(_store.tickets[i].id == action.response.id){
            _store.tickets[i] = action.response;
          }
        }
      }
    break;

    case TicketConstants.ADD_TICKET:
      //dodanie do naszego tablicy z ticketami danych z ticketa
      //action.data to wynik argument który przekazujemy do Dispatcher
      //po drodze można go obrabiać ale tutaj nie ma po co
      if(action.data.ticket){
      _store.tickets.push(action.data.ticket);
      //najważniejsze czyli emisja zmiany magazynu na jej podstawie funkcje addChangeListener
      //wywołają ponowny render componentu przez co zmiany bedą widoczne
      TicketStore.emit(CHANGE_EVENT);
      }
    break;

    case TicketConstants.UPDATE_TICKET:
    //  _store.tickets[action.ticket.id] = action.ticket;
    console.info("UPDATE");
      TicketStore.emit(CHANGE_EVENT);
    break;

    case TicketConstants.DELETE_TICKET:
      _store.tickets = _store.tickets.filter((item, index) => {
      return index !== action.index;
    });
       TicketStore.emit(CHANGE_EVENT);
    break;

    default:
    return true
  }
});

export default TicketStore;
