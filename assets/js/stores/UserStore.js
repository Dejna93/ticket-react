import AppDispatcher from '../dispatcher/AppDispatcher';
import {UserConstants} from '../constants/UserConstants';
import {EventEmitter} from 'events';

const CHANGE_EVENT = 'change';

let _user = {
  username : '',
  password : '',
  token : '',
  is_staff : '',
  email : '',
};

let _error = {
  message : '',
  title : '',
  status : '',
};


class UserStoreClass extends EventEmitter {


  addChangeListener(cb) {
    this.on(CHANGE_EVENT, cb);
  }

  removeChangeListener(cb) {
    this.removeListener(CHANGE_EVENT, cb);
  }

  isLogin(){
    return ( _user.token !== '') ? true: false;
  }
  cleanUser(){
    _user.username ="";
    _user.password ="";
    _user.token = "";
    _user.staff = "";
    this.cleanError();

  }

  getUser(){
    return _user;
  }
  getToken(){
    return _user.token;
  }
  getUserName(){
    return _user.username;
  }
  getError(){
    return _error;
  }
  cleanError(){
    _error.message = "";
    _error.status = "";
  }
}

const UserStore = new UserStoreClass();

AppDispatcher.register( (payload) => {

    const action = payload.action;
    switch (action.actionType) {

      case UserConstants.LOGIN_API:
          _user.username = action.response.username;
          _user.token = action.response.token;
          _user.password = action.data;
          _user.is_staff = action.is_staff;
          UserStore.cleanError();
          UserStore.emit(CHANGE_EVENT);
          break;
      case UserConstants.USER:

          _user.username = action.response.data.username;
          _user.email = action.response.data.email;
          _user.is_staff = action.response.data.is_staff;
          UserStore.emit(CHANGE_EVENT);
          break;
      case UserConstants.LOGIN:
          console.info(_user);
          UserStore.emit(CHANGE_EVENT);
        break;
      case UserConstants.LOGOUT:
          _user = UserStore.initUser();
          UserStore.emit(CHANGE_EVENT);
          break;
      case UserConstants.ERROR:
          console.info("ERROR STORE = "+action.response);
          _error.message = "Błąd logowania.\n Sprawdź poprawność loginu i hasła";
          _error.status = action.response;
          _error.title = "Błąd logowania";
          break;
      default:
          return true;
    }

})
export default UserStore;
