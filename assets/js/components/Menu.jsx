import React , {Component} from 'react';
import {Navbar, Nav ,NavItem ,NavDropdown, MenuItem } from 'react-bootstrap';
import {LinkContainer } from 'react-router-bootstrap';
import {logout} from '../utils/Utils.js';
import UserStore from '../stores/UserStore.js';

export default class Menu extends Component{

  constructor(props){
    super(props);
    this.logOut= this.logOut.bind(this);
  }

  logOut(){
    console.info("LOGOUT");
    logout();
  }

  render(){
    return(
      <div>
      <Navbar inverse>
         <Navbar.Header>
           <Navbar.Brand>
             <a href="#">Ticket System</a>
           </Navbar.Brand>
           <Navbar.Toggle />
         </Navbar.Header>
         <Navbar.Collapse>
           <Nav>
             <LinkContainer to={{ pathname: '/'}}>
               <NavItem>Home</NavItem>
             </LinkContainer>

           </Nav>
           <Nav pullRight>
           { UserStore.isLogin() ?
               <NavItem onClick={this.logOut}>LOGOUT</NavItem>
             : null
           }
           </Nav>
         </Navbar.Collapse>

       </Navbar>
       </div>
    );
  }


}
