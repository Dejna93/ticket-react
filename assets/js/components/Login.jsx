import React , {Component} from 'react';
import LinkedStateMixin from 'react-addons-linked-state-mixin';
import { Grid ,Row, Col , ClearFix , Modal} from 'react-bootstrap';
import { Panel , Form , FormGroup , FormControl, ControlLabel, Button, ButtonToolbar} from 'react-bootstrap';
import {loginUser}  from '../actions/UserAction.js';
import UserStore from '../stores/UserStore.js';
import TicketStore from '../stores/TicketStore.js';
import Error from './Error.jsx';
import {viewUser} from '../actions/UserAction.js';
export default class Login extends Component{

  constructor(props){
    super(props);
    this.login = this.login.bind(this);
    this.loginModal = this.loginModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    //this._onChange = this._onChange.bind(this);
    this.state = {
      username : '',
      password : '',
      showModal : true,
      isLogin  : false,
      error : {},
    }

  }


  componentDidMount(){
    console.info("mount");
    var self = this;
    this.loadInteval = setInterval(function(){
      console.info(UserStore.isLogin());
      self.setState({
        error: UserStore.getError(),
        isLogin : UserStore.isLogin(),
      });
    }, 100);

  }
  componentWillUnmount(){
    this.loadInteval && clearInterval(this.loadInteval);
    this.laodInterval = false;
  }
  closeModal(){

    if(this.state.isLogin){
      console.info('close');
      this.setState({
        showModal : false,
      });
    }
  }
  loginModal(){
    this.setState({
      showModal : true,
    });
  }

  handleChange (key){
    return function(e){
      var change = {};
      change[key] = e.target.value;
      this.setState(change);
    }.bind(this);
  }


  login(){
    console.info('CLICK');
    var data = {
        username : this.state.username,
        password : this.state.password,
      };
    loginUser(data);
    setTimeout(function(){
        viewUser(UserStore.getUser().username);
    },500);
    this.closeModal();
  }



  render(){


    //console.log(JSON.stringify(this.state, null, 4));
    return(
      <div>
        <Modal show={this.state.showModal}  onHide={this.closeModal}>

          <Modal.Header>
            <h2 className="text-center">Login</h2>
          </Modal.Header>
          { this.state.error.status == "400" ? <Error title={this.state.error.title} message={this.state.error.message} />: null}
          <Form horizontal>
            <Modal.Body>
              <FormGroup controlId="formLogin">
                <Col compotentClass={ControlLabel} sm={2}>
                  Login
                </Col>
                <Col sm={10}>
                  <FormControl type="text" placeholder="Login" controlId="username" value={this.state.username} onChange={this.handleChange('username')} />
                </Col>
              </FormGroup>

              <FormGroup controlId="formPassword">
                  <Col componentClass={ControlLabel} sm={2}>
                    Password
                  </Col>
                  <Col sm={10}>
                    <FormControl type="password" placeholder="Password" controlId="password" value={this.state.password} onChange={this.handleChange('password')}  />
                  </Col>
              </FormGroup>
              </Modal.Body>
              <Modal.Footer>
              <FormGroup>
                <Col smOffset={2} sm={10}>
                  <Button  onClick={this.login}>
                    Sign in
                  </Button>
                </Col>
              </FormGroup>
              </Modal.Footer>
          </Form>

        </Modal>

        </div>

    );
  }

}
