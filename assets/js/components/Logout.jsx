import React , {Component} from 'react';
import {logout} from '../actions/UserAction';
export default class Logout extends Component{

  componentDidMount(){
    console.info('LOGOUT');
    logoutUser();
  }
}
