import React , {Component } from 'react';
import { Grid ,Row, Col , ClearFix } from 'react-bootstrap';
import {Table, Button, ButtonGroup , Glyphicon,Modal, Form , FormGroup , FormControl, ControlLabel } from 'react-bootstrap';
import {Navbar, Nav ,NavItem ,NavDropdown, MenuItem }from 'react-bootstrap';

import { updateTicket} from '../actions/TicketActions.js';
import UserStore from '../stores/UserStore.js';

export default class State extends Component{

  constructor(props){
    super(props);
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.changeState = this.changeState.bind(this);
    this.state = {
      show : this.props.show,
      ticket : this.props.ticket,
    };
  }
  handleChange(key){
    //TODO KOPIOWANIE TICKETA I AKUTALIZACJA DANEGO KLUCZA!!!!
    return function(e){
      var cloneTicket  = (this.state.ticket);
      cloneTicket[key] = e.target.value;
      this.setState({ ticket : cloneTicket});
    }.bind(this);
  }
  open(){
    this.setState({show: true});
  }
  close(){
      this.setState({show: false});
  }
  changeState(){
    updateTicket(  this.state.ticket, UserStore.getUser().token,  UserStore.getUser().username   );
    this.setState({
      show :false
    });
  }
  render(){
    if (this.state.show != true){
      return <Button bsStyle="primary" onClick={this.open} >Change status</Button>
    }
    return(
      <Modal show={this.state.show} onHide={this.close}>

        <Modal.Header>
          <h3 className="text-center">Change state</h3>
        </Modal.Header>

        <Form horizontal>
        <Modal.Body>
          <Col componentClass={ControlLabel} sm={2}> State</Col>
          <Col sm={10}>
            <FormControl componentClass="select" onChange={this.handleChange('state')}>
              <option value="Nowy">Nowy</option>
              <option value="W przygotowaniu">W przygotowaniu</option>
              <option value="Opóźnione">Opóźnione</option>
              <option value="Testy">Testy</option>
              <option value="Zamknięty">Zamknięty</option>
            </FormControl>
          </Col>
        </Modal.Body>
        <Modal.Footer>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" bsSize="large" onClick={this.changeState}>Change state</Button>
          </Col>
        </Modal.Footer>
        </Form>

      </Modal>
    );
  }
}
