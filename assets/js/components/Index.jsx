// React components
import React from 'react';
import ReactDOM from 'react-dom';

import {Router, Route, DefaultRoute, browserHistory} from 'react-router';
import TicketApp from './TicketApp.jsx';
import Tickets from './Tickets.jsx';
import Ticket from './Ticket.jsx';
import Login from './Login.jsx';
import Archives from './Archives.jsx';
import NotFound from './NotFound.jsx';


ReactDOM.render((
  <Router history={browserHistory}>
    <Route path="/" component={TicketApp} />
    <Route path="ticket:id" component={Ticket}/>
    <Route path="archive" compontent={Archives}/>
    <Route path="login" component={Login}/>
    <Route path="logout" component={Login}/>

    <Route path='*' component={NotFound}/>


  </Router>
), document.getElementById("app"))
