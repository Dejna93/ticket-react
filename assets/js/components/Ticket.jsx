import React , {Component } from 'react';
import { Grid ,Row, Col , ClearFix } from 'react-bootstrap';
import {Table, Button, ButtonGroup , Glyphicon } from 'react-bootstrap';
import {Navbar, Nav ,NavItem ,NavDropdown, MenuItem }from 'react-bootstrap';
import {LinkContainer } from 'react-router-bootstrap';
import {Panel} from 'react-bootstrap';
import TicketStore from '../stores/TicketStore.js';
import Menu from './Menu.jsx';
import { updateTicket} from '../actions/TicketActions.js';
import { deleteTicket } from '../actions/TicketActions.js';
import {getTicket} from '../actions/TicketActions.js';
import UserStore from '../stores/UserStore.js';
import EditTicket from './EditTicket.jsx';
import { browserHistory } from 'react-router';
import State from './State.jsx';

export default class Ticket extends Component {

  constructor(props){
    super(props);
    var ticket = {};
    this._archive = this._archive.bind(this);
    this._change = this._change.bind(this);
    this._delete = this._delete.bind(this);
    this.back = this.back.bind(this);
    this.state = {
      ticket : {}
    };
    if(this.props.location.query.id){
        console.info(this.props.location.query.id);
        this.state.ticket = TicketStore.getTicket(this.props.location.query.id);
    }
  }
  componentDidMount(){
    var self = this;

    this.loadTicketInterval = setInterval(function(){
      if(UserStore.getUser().username){
        getTicket(self.state.ticket.id);
          self.setState({
            tiicket : TicketStore.getTicket(self.props.location.query.id),
          });
      }
    },500);
  }
  componentWillUnmount(){
    clearInterval(this.loadTicketInterval);
    this.loadTicketInterval = false;
  }
  back(){
    browserHistory.push("/");
  }

  _delete(){
    deleteTicket(this.state.ticket.id);
    browserHistory.push("/");
   }
  _archive(){
      console.info("Archive");
      var ticket = this.state.ticket;
      ticket.state ="Zamknięty";
      console.info(ticket);
      updateTicket(ticket, UserStore.getToken(), UserStore.getUser());

  }
  _change(){
      console.info("change-status");
  }
  render() {
    var ticket = {
      "id" : this.state.ticket.id,
      "title": this.state.ticket.title,
      "state" : this.state.ticket.state,
      "type": this.state.ticket.type,
      "content" : this.state.ticket.content,
      "create" : this.state.ticket.create,
      "user"  : this.state.ticket.user.username,
    };
    var  {id,title,state,type,content,create,user} = ticket;

    return(
      <div>
      <Menu />
      <Grid>
      <Panel header={"Ticket numer "+id}>
      <Row>
      <Col sm={8} smOffset={1}>
        <Col sm={6}>Title</Col>
        <Col sm={4}>Type</Col>
      </Col>


      </Row>
      <Row>
      <Col sm={8} smOffset={1}>
        <Col sm={8}><div className="well">{title ? title : '-'}</div></Col>
        <Col sm={4}><div className="well">{type ? type : '-'}</div></Col>
      </Col>
      </Row>
      <Row>
      <Col sm={8} smOffset={1}>
          <Col sm={3}>State</Col>
          <Col sm={3}>Create time</Col>
          <Col sm={3}>Delay</Col>
          <Col sm={3}>Create by</Col>
      </Col>
      </Row>
      <Row>
      <Col sm={8} smOffset={1}>
        <Col sm={3}><div className="well">{state ? state : '-'}</div></Col>
        <Col sm={3}><div className="well">{create ? create : '-'}</div></Col>
        <Col sm={3}><div className="well">{create ? create : '-'}</div></Col>
        <Col sm={3}><div className="well">{user ? user : '-'}</div></Col>
      </Col>
      </Row>
      <Row>
        <Col sm={10} smOffset={1}>Content</Col>
      </Row>
      <Row>
        <Col sm={10} smOffset={1}>
        <div className="well">{content ? content : '-'}</div>

        </Col>
      </Row>
      <Row>
        <Col sm={6} smOffset={1}>
          <ButtonGroup>
          { UserStore.getUser().is_staff ?
            <div>
              <State ticket={this.state.ticket} show={false}/>
              <Button onClick={this.back}>Back</Button>

              </div>
            :
            <div>
              <EditTicket showModal={false} edit="1" id={this.props.location.query.id} />
              <Button bsSize="small" onClick={this._delete}>Delete ticket</Button>
              <Button onClick={this.back}>Back</Button>

              </div>
          }

          </ButtonGroup>
        </Col>
      </Row>
      </Panel>
      </Grid>

      </div>


    );
  }

}
