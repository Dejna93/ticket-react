import React from 'react';

import UserStore from '../stores/UserStore.js';
import TicketStore from '../stores/TicketStore.js';

import AddTicket from './AddTicket.jsx';
import Login from './Login.jsx';
import Tickets from './Tickets.jsx';
import Menu from './Menu.jsx';
import EditTicket from './EditTicket.jsx';

import { Grid ,Row, Col , ClearFix, Modal, Button, Panel, Table , ListGroup , ListGroupItem} from 'react-bootstrap';
import {Link} from 'react-router';
import {getTicketByState, getTicketsByUser} from '../actions/TicketActions.js';
import {viewUser} from '../actions/UserAction.js';

export default class TodoApp extends React.Component {

  constructor(props) {
    super(props);
    this._onChange = this._onChange.bind(this);

// inicjalizacja stanu componentu
    this.state =  {
      tickets : TicketStore.getAllTickets(),
      isAuthenticated : UserStore.isLogin(),
    };

  }


//te funkcje masz DOC wyjaśnione
  componentDidMount() {
    TicketStore.addChangeListener(this._onChange);
    var self = this;

    this.loadTicketInterval = setInterval(function(){

              //filtrowanie ticketow nie zamknietych i uzytkownika
        //  console.info("user = "+UserStore.getUser().username);
          if(UserStore.getUser().username){
            viewUser(UserStore.getUser().username);
          }
          if(UserStore.getUser().username && UserStore.getUser().is_staff == false){
            getTicketsByUser(UserStore.getUser().username);
          }
          if(UserStore.getUser().username && UserStore.getUser().is_staff){
              getTicketByState(5);
          }

          if(this.location.pathname == "/"){
            self.setState({
                 tickets : TicketStore.getAllTickets(),
                 isAuthenticated: UserStore.isLogin(),
               });
          }

        },1000);



  }
//te funkcje masz DOC wyjaśnione
  componentWillUnmount() {

    TicketStore.removeChangeListener(this._onChange);
    clearInterval(this.loadTicketInterval);
    this.loadTicketInterval = false;
    console.info(UserStore.getUser().username + " " +UserStore.getUser().is_staff );
  }

//funkcja która wyzwala aktualizacje staChvrches nu w compontencie
//czyli pobieranie listy ticketów z magazynu
  _onChange() {
    this.setState({
        tickets : TicketStore.getStore(),
    });
  }

  render() {
  //  this.state                  <Tickets tickets={Ticket.loggedIn = UserStore.isLogin();
    return (
      <div>

          <Menu />
          <Grid>
            <Row>
              <Col xd={12} sm={10} smOffset={1}>
              {this.state.isAuthenticated ?
              <Panel>Witam użytkownika {UserStore.getUser().username}
                {UserStore.getUser().is_staff ?
                   <p>Panel obsługi ticketów klientów. Sprawdź najnowsze tickety i zaktualizuj statusy już zakończonych</p> : null}</Panel> :
              <Login />}
              {UserStore.getUser().is_staff ?
                <Panel> Legenda
                  <Table horizontal>
                    <tr>
                      <td>Nowy</td>
                      <td className="success"></td>
                    </tr>
                    <tr>
                      <td>Opóźniony</td>
                      <td className="warning"></td>
                    </tr>
                    <tr>
                      <td>Zamknięty</td>
                      <td className="danger"></td>
                    </tr>


                  </Table>
                 </Panel>
                :null}
                <Col sm={5} smOffset={4}>
                {UserStore.getUser().is_staff==false ?  <AddTicket show={false}/> : null}

                </Col>

             </Col>
               <Col xd={12} sm={10} smOffset={1}>

                  <Tickets tickets={TicketStore.getStore()}/>
               </Col>
            </Row>
          </Grid>


      </div>

    );
  }
}
