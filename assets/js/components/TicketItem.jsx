import React, {Component} from 'react';
import {Table, Button, ButtonGroup , Glyphicon } from 'react-bootstrap';
import {Link } from 'react-router';
import { deleteTicket } from '../actions/TicketActions.js';
import EditTicket from './EditTicket.jsx';
import UserStore from '../stores/UserStore.js';
import {colorTable} from '../utils/Utils.js';

export default class TicketItem extends React.Component {

  constructor(props){
    //bez tego konstuktora nie bedzie widzieć props
    super(props);
    this._delete = this._delete.bind(this);
  }

  _delete(){
    deleteTicket(this.props.keys);
  }

  render(){
    //  <Button bsSize="small"><Glyphicon glyph="edit" /></Button>
    return(
      <tr className={colorTable(this.props.state)}>
      <td>{this.props.id}</td>
      <td>{this.props.state}</td>
      <td>{this.props.publish}</td>
      <td>{this.props.title}</td>
      {UserStore.getUser().is_staff ? <td>{this.props.user}</td> : null}
      <td>
      <Link to={{ pathname: '/tickets/', query: { id: this.props.id } }}><Button bsSize="small"><Glyphicon glyph="eye-open" /></Button></Link>
      { UserStore.getUser().is_staff == false ?
      <EditTicket showModal={false} id={this.props.id} />
      : null }
      { UserStore.getUser().is_staff == false ?
      <Button bsSize="small" onClick={this._delete}><Glyphicon glyph="remove" /></Button>
      : null
      }

       </td>
      </tr>

    )
  }
}
