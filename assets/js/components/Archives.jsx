import React, {Component} from 'react';
import UserStore from '../stores/UserStore.js';
import TicketStore from '../stores/TicketStore.js';
import Tickets from './Tickets.jsx';
import {getArchives} from '../actions/TicketActions.js';
import { Grid ,Row, Col , ClearFix } from 'react-bootstrap';
import {Table, Button, ButtonGroup , Glyphicon } from 'react-bootstrap';
import {Navbar, Nav ,NavItem ,NavDropdown, MenuItem }from 'react-bootstrap';
//getArchiveTicketsAPI

export default class Archive extends Component{

  constructor(props){
    super(props);
    console.info("asdasdasdsarchive");
    this.state = {
      tickets : TicketStore.getAllTickets(),
      user : UserStore.getUser()
    };
  }
  componentDidMount(){
    var self = this;
    this.loadTicketInterval = setInterval(function(){
      if(self.state.user){
        getArchives(self.state.user.username);
        self.setState({
          user : UserStore.getUser(),
          tickets: TicketStore.getAllTickets()
        });
      }
    })
  }
  componentWillUnmount(){

  }
  render(){
    return(
      <div>
        <Menu/>
        <Grid>
          <Row>
            <Col xd={12} sm={10} smOffset={1}>
              <Tickets tickets={this.state.tickets} />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }


}
