import React , {Component} from 'react';
import { Grid ,Row, Col , ClearFix,  Panel} from 'react-bootstrap';

export default class Error extends Component {

  constructor(props){
    super(props);
  }

  render(){
    return(
    <Panel header={this.props.title} bsStyle="danger" className="text-center">
       <p className="text-center">{this.props.message}</p>
     </Panel>
   );
  }
}
