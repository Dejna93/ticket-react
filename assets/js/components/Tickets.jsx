import React , {Component} from 'react';

import { Grid ,Row, Col , ClearFix } from 'react-bootstrap';
import {Table, Button, ButtonGroup , Glyphicon } from 'react-bootstrap';

import TicketItem from './TicketItem.jsx';
import UserStore from '../stores/UserStore.js';
export default class Tickets extends Component {


  render() {
    let rows = [];


      //dla niepustych danych stworzy sobie listy compotentów mapująć sobie tą tablice
      //item obiekt zawiera dane pojedynczego ticketu z magazynu
      //index wiadomo
      if(this.props.tickets){
        this.props.tickets.map((item ,index) =>{
          rows.push(<TicketItem id={index} keys={item.id}  title={item.title} state={item.state} type={item.type} content={item.content} publish={item.create} user={item.user.username} />)

        });

      }


    return (
  //    { test ?
  //        <Ticket id={test.id} title={test.title} state={test.state} type={test.state} content={test.content} publish={test.publish} />
//:
//null
    //    }
      <div>

          <table className="table table-bordered ">
            <thead>
              <tr className="info">
                <th>Id</th>
                <th>State</th>
                <th>Publish</th>
                <th>Title</th>
                {UserStore.getUser().is_staff ? <th>Client</th> : null}
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {rows}
            </tbody>
          </table>

      </div>

    );
  }

}
