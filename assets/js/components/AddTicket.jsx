import React , {Component} from 'react';
import { Grid ,Row, Col , ClearFix , Modal} from 'react-bootstrap';
import { Panel , Form , FormGroup , FormControl, ControlLabel, Button, ButtonToolbar} from 'react-bootstrap';
//akcje
import { saveTicket} from '../actions/TicketActions.js';
import TicketStore from '../stores/TicketStore.js';
import UserStore from '../stores/UserStore.js';

export default class AddTicket extends Component{

  constructor(props){
    super(props);
    this._add = this._add.bind(this);
    this.state = {
      ticket : {
        id: '',
        title: '',
        state : 'Nowy',
        type : '',
        content : '',
        create : '',
        user : '',
      },
      showModal : this.props.show,
    };

    this.openModel = this.openModel.bind(this);
    this.closeModal = this.closeModal.bind(this);

  }
  closeModal(){
    this.setState({showModal: false});
  }
  openModel(){
    this.setState({
      showModal: true,
      ticket : {},
      error : false,
    });
  }


  _add(){
    console.info(this.state.ticket.state  + " " + this.state.ticket.type);
  //  if(this.validation()){
    if(this.state.ticket.state  === undefined ){
        var temp = this.state.ticket;
      temp.state = "1";
      this.setState({ticket: temp});
    }
    if( this.state.ticket.type === undefined  ){
        var temp = this.state.ticket;
        temp.type ="1";
        this.setState({ticket: temp});
    }
    saveTicket(this.state.ticket,UserStore.getUser());
    this.setState({
      showModal: false,
      error : false,
    });
    //}else{
    //  this.setState({error : true})
    //}
  }
  handleChange (key){
    //TODO KOPIOWANIE TICKETA I AKUTALIZACJA DANEGO KLUCZA!!!!
    return function(e){
      var cloneTicket  = (this.state.ticket);
      cloneTicket[key] = e.target.value;
      this.setState({ ticket : cloneTicket});
    }.bind(this);

  }
  getState(){
    return this.ticket;
  }


  render(){
    if (this.state.showModal != true){
      return(
      <Panel>
      <p>Proszę pamiętać o ważnych umowach serwisowych przed dodaniem zgłoszenia serwisowego.</p>
      <Col smOffset={4}>
      <Button bsStyle="primary" bsSize="xlarge" onClick={this.openModel} >Add ticket</Button>

      </Col>

      </Panel>
    );
    }
    return(
      <div>

       <Modal show={this.state.showModal} onHide={this.closeModal}>

        <Modal.Header closeButton>
          <h2 className="text-center">Add ticket</h2>
        </Modal.Header>
        <div >
          <p>{this.state.error ? "ERROR IN FORM" : ""}</p>
        </div>
        <Form horizontal>
            <Modal.Body>
          <FormGroup controlId="">
            <Col componentClass={ControlLabel} sm={2}>
              Titile
            </Col>
            <Col sm={10}>
              <FormControl  placeholder="Title" value={this.state.ticket.title} onChange={this.handleChange('title')} />
            </Col>
          </FormGroup>

          <FormGroup controlId="">
            <Col componentClass={ControlLabel} sm={2}>
              State
            </Col>
            <Col sm={10}>
            <FormControl  value={this.state.ticket.state} readOnly onChange={this.handleChange('state')}>

            </FormControl>
            </Col>
          </FormGroup>

          <FormGroup controlId="">
            <Col componentClass={ControlLabel} sm={2}>
              Type
            </Col>
            <Col sm={10}>
            <FormControl componentClass="select" value={this.state.ticket.type} placeholder="select" onChange={this.handleChange('type')}>
              <option value="1">Serwisowe</option>
              <option value="2">Projekt wewnętrzny</option>
              <option value="3">Projekt zewnętrzny</option>
            </FormControl>
            </Col>
          </FormGroup>

          <FormGroup controlId="formControlsTextarea">
          <Col componentClass={ControlLabel} sm={2}>
           Content
          </Col>
          <Col sm={10}>
            <FormControl componentClass="textarea" placeholder="textarea"  value={this.state.ticket.content} onChange={this.handleChange('content')}/>
            </Col>
          </FormGroup>

          <FormGroup controlId="">
            <Col componentClass={ControlLabel} sm={2}>
              Publish
            </Col>
            <Col sm={10}>
              <FormControl type="date" placeholder="publish" value={this.state.ticket.publish} onChange={this.handleChange('publish')} />
            </Col>
          </FormGroup>


          </Modal.Body>
          <Modal.Footer>
          <FormGroup>
            <Col smOffset={2} sm={10}>
             <ButtonToolbar>
              <Button onClick={this._add}  bsStyle="primary">
                Add ticket
              </Button>
              <Button   bsStyle="info">
                Update
              </Button>
               </ButtonToolbar>
            </Col>
          </FormGroup>
          </Modal.Footer>

        </Form>
        </Modal>
      </div>
    );
  }

}
