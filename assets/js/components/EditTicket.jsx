import React , {Component} from 'react';
import { Grid ,Row, Col , ClearFix , Modal, Glyphicon} from 'react-bootstrap';
import { Panel , Form , FormGroup , FormControl, ControlLabel, Button, ButtonToolbar} from 'react-bootstrap';
//akcje
import { updateTicket} from '../actions/TicketActions.js';
import TicketStore from '../stores/TicketStore.js';
import UserStore from '../stores/UserStore.js';

export default class EditTicket extends Component{

//get id from view
    constructor(props){
      super(props);
      this._update = this._update.bind(this);
      this.state = {
        show: this.props.showModal,
        ticket: TicketStore.getTicket(this.props.id),
        auth : UserStore.getToken(),
      };
      this.open = this.open.bind(this);
      this.close = this.close.bind(this);

    }
    componentDidMount(){
      //var self = this;
    //  setInterval(function(){
      //  self.setState({
      //    auth : UserStore.getToken(),
      //  });
  //    },10000);
    }

    open(){
      console.info('open');
      this.setState({
        show : true,
      });
    }
    close(){
      this.setState({
        show : false,
      });
    }
    _update(){
      var ticket = this.state.ticket;
      console.info(ticket.state +  " " +ticket.type )
      updateTicket(  this.state.ticket, this.state.auth,  UserStore.getUser().username   );
      this.setState({
        ticket : this.state.ticket,
        error : false,
      });
      this.close();
    }
    handleChange (key){
      //TODO KOPIOWANIE TICKETA I AKUTALIZACJA DANEGO KLUCZA!!!!
      return function(e){
        var cloneTicket  = (this.state.ticket);
        var change = {};
        change[key] = e.target.value;
        cloneTicket[key] = e.target.value;
        this.setState({ ticket : cloneTicket});
      }.bind(this);

    }

    render(){
      if (this.state.show != true && this.props.edit ){
        return (
          <Button bsSize="small" bsStyle="primary" onClick={this.open} >Edit ticket</Button>

        )
      }
      if (this.state.show != true){
        return <Button bsSize="small" onClick={this.open} ><Glyphicon glyph="edit" /></Button>
      }



      return(
        <div>

         <Modal show={this.state.show} onHide={this.close}>

          <Modal.Header closeButton>
            <h2 className="text-center">Edit ticket</h2>
          </Modal.Header>

          <Form horizontal>
              <Modal.Body>
            <FormGroup controlId="">
              <Col componentClass={ControlLabel} sm={2}>
                Titile
              </Col>
              <Col sm={10}>
                <FormControl  placeholder="Title" value={this.state.ticket.title} onChange={this.handleChange('title')} />
              </Col>
            </FormGroup>

            <FormGroup controlId="">
              <Col componentClass={ControlLabel} sm={2}>
                State
              </Col>
              <Col sm={10}>
              <FormControl value={this.state.ticket.state}  readOnly onChange={this.handleChange('state')}>
              </FormControl>
              </Col>
            </FormGroup>

            <FormGroup controlId="">
              <Col componentClass={ControlLabel} sm={2}>
                Type
              </Col>
              <Col sm={10}>
              <FormControl value={this.state.ticket.type} readOnly onChange={this.handleChange('type')}>

              </FormControl>
              </Col>
            </FormGroup>

            <FormGroup controlId="formControlsTextarea">
            <Col componentClass={ControlLabel} sm={2}>
             Content
            </Col>
            <Col sm={10}>
              <FormControl componentClass="textarea" placeholder="textarea"  value={this.state.ticket.content} onChange={this.handleChange('content')}/>
              </Col>
            </FormGroup>




            </Modal.Body>
            <Modal.Footer>
            <FormGroup>
              <Col smOffset={2} sm={10}>
               <ButtonToolbar>

                <Button onClick={this._update}  bsStyle="info">
                  Update
                </Button>
                 </ButtonToolbar>
              </Col>
            </FormGroup>
            </Modal.Footer>

          </Form>
          </Modal>
        </div>
      );




    }


}
